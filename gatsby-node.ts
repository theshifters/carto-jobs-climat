//https://gist.github.com/JohnAlbin/2fc05966624dffb20f4b06b4305280f9

// Because we used ts-node in gatsby-config.js, this file will automatically be
// imported by Gatsby instead of gatsby-node.js.

// Use the type definitions that are included with Gatsby.
import { GatsbyNode } from "gatsby";
import { resolve } from "path";
import { Tree, Category, allCategoriesType, allOrganizationsType, Organization, allDispatchFormType, DispatchForm } from "./src/utils/model"


type allPagesGraphql = {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    errors?: any;
    data?: {
        categories?: allCategoriesType,
        organizations?: allOrganizationsType;
        forms?: allDispatchFormType;
    };
}

export const createPages: GatsbyNode["createPages"] = async ({
    actions,
    graphql,
}) => {
    const { createPage } = actions;

    const allPages: allPagesGraphql = await graphql(`
    query MyQuery {
        categories: allAirtable(filter: {table: {eq: "Category"}, data: {Name: {ne: null}, Active: {eq: true}}}) {
            nodes {
                id
                data {
                    Order
                    Name
                    OrgCount
                    HasDedicatedPage
                    Organizations{
                        id
                        data {
                            Name
                            ActivityDescription {
                                childMarkdownRemark {
                                  html
                                }
                            }
                            Categories {
                                id
                                data {
                                    Name
                                    Order
                                }
                            }
                            StructureType
                            CreationYear
                            Workforce
                            Headquarter {
                                id
                                data {
                                    CityName
                                    GPSCoordinates
                                    DepartmentName
                                    RegionName
                                    Dpt2
                                }
                            }
                            City {
                                id
                                data {
                                    CityName
                                    GPSCoordinates
                                    DepartmentName
                                    RegionName
                                    Dpt2
                                }
                            }
                            idMakesenseValue
                            Linkedin
                            Website
                            Tags {
                                id
                                data {
                                    Tag
                                }
                            }
                            Lever {
                                id
                                data {
                                    Name
                                }
                            }
                            Trainings {
                                id
                                data {
                                    Training
                                }
                            }
                            Professions {
                                id
                                data {
                                    Profession
                                }
                            }
                        }
                    }
                }
            }
        }
        organizations: allAirtable(filter: { table: { eq: "Organization" }, data: {Status: {eq: "Validé"}}} ) {
            nodes {
                id
                data {
                    Name
                }
            }
        }
        forms: allAirtable(filter: { table: { eq: "DispatchForm" }} ) {
            nodes {
                id
                data {
                    Name
                }
            }
        }
    }
    `)

    let categories = new Tree('/categories', allPages.data?.categories.nodes, 'Secteurs');
    categories.walk((node: Category): void => {
        if (node.hasDedicatedPage) {
            createPage({
                path: `${node.slug}/`,
                component: resolve(`./src/templates/category.tsx`),
                context: {
                    categoryId: node.id,
                    airtableId: node.airtableId
                },
            })
        }
    });
    
    categories.walk((node: Category): void => {
            let organizations = node.totalOrganizations.map(org => {
                return {...org, slug: org.slug}
            });
            let context = {
                categoryId: node.id,
                airtableId: node.airtableId,
                organizations
            }

            if (node.searchSlug) {

                createPage({
                    path: `${node.searchSlug}/`,
                    component: resolve(`./src/pages/search.js`),
                    context
                })
            }
        
    });

    allPages.data?.organizations?.nodes.forEach(org => {

        if (null !== org.data.Name) {
            let organization = new Organization({ id: org.id, name: org.data.Name })
            createPage({
                path: `${organization.slug}/`,
                component: resolve(`./src/templates/organization.tsx`),
                context: {
                    organizationId: organization.id,
                }
            })
        }
    })


    allPages.data?.forms?.nodes.forEach(org => {
        if (null !== org.data.Name) {
            let form = new DispatchForm(org.id, org.data.Name)
            createPage({
                path: form.slug,
                component: resolve(`./src/templates/form.tsx`),
                context: {
                    formId: form.id,
                }
            })
        }
    })
};

// https://www.gatsbyjs.com/docs/debugging-html-builds/
exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
    if (stage === "build-html" || stage === "develop-html") {
      actions.setWebpackConfig({
        module: {
          rules: [
            {
              test: /leaflet\.markercluster|leaflet-src/,
              use: loaders.null(),
            },
          ],
        },
      })
    }
  }