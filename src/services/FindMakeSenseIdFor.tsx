//deprecated
class FindMakeSenseIdFor{
    ids_make_sense_data: any
    constructor(ids_make_sense_data){
        this.ids_make_sense_data = ids_make_sense_data
    }
    process(shiftYourJobId){
        let makesenseID = null
        for (let index = 0; index < this.ids_make_sense_data.length; index++) {
          const element = this.ids_make_sense_data[index]
          if (shiftYourJobId === element.data.ShiftYourJobId) {
            makesenseID = element.data.idMakesense
          }
        }
        return makesenseID;
    }
}
export default FindMakeSenseIdFor;