import React from "react"
import { Link } from "gatsby"

export default function TopicCard({ category }) {
  return (
    <div className="w-full sm:w-1/2 md:w-1/4 p-3">
      <Link
        to={category.slug}
        className="block rounded overflow-hidden shadow-md relative"
      >
{/*         
        {category.cover && (
          <Img
            fluid={category.cover}
            className="w-full h-40 object-cover pointer-events-none select-none"
          />
        )} */}
        <div className="px-3 py-2 w-full text-xl font-bold  text-black tracking-wide">
          {category.name}
        </div>
      </Link>
    </div>
  )
}
