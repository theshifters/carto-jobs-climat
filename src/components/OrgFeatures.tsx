import React, {useMemo} from "react"
import {Organization} from "../utils/model"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faUserFriends, faBookmark, faMapMarkerAlt } from "@fortawesome/free-solid-svg-icons"
import {FULL_COUNTRY_ORGANIZATION_CITY} from "./OrganizationsMap/OrganizationsMap";
import makesenseIcon from "../images/jobs_that_makesense.png"

type OrgFeaturesProps = {
    organization: Organization
    className: string
}

function sortOfficeCities(officeCities: string[]){
    const copiedOfficeCities = [...officeCities]
    return copiedOfficeCities.sort((officeCityA, officeCityB) => {
        if (officeCityA === FULL_COUNTRY_ORGANIZATION_CITY){
            return 1 //always place full country organization city at the end
        }
        return officeCityA > officeCityB ? 1 : -1
    })
}


const OrgFeatures = ({ organization, className }: OrgFeaturesProps) => {

    const locationsString = useMemo(() =>{
        if (organization.officeCities.length === 0) {
            return organization.headquarter
        }
        return `${organization.headquarter} et ${sortOfficeCities(organization.officeCities).join(", ")}`
    }, [organization.officeCities, organization.headquarter]);

    const makeSense = organization.idMakeSense

    return (
        <div className={"flex flex-wrap  " + className}>
            { Number.isInteger(organization.creationYear) && (
            <div className="mr-5">
                Créé en { organization.creationYear}
            </div>
            )}
            { organization.effectif  && (
            <div  className="mr-5">
                <FontAwesomeIcon
                    icon={faUserFriends}
                    className="mr-1 "
                />
                { organization.effectif }
            </div>
            )}
            {organization.structure && (
            <div  className="mr-5">
                <FontAwesomeIcon
                    icon={faBookmark}
                    className="mr-1 "
                />
                { organization.structure }
            </div>
            )}
            {locationsString && (
            <div  className="mr-5">
                <FontAwesomeIcon
                    icon={faMapMarkerAlt}
                    className="mr-1"
                />
                <span>{locationsString}</span>
            </div>
            )}

            {makeSense !== null && (
            <div  className="mr-5 flex flex items-center">
                    <img
                    className="mr-1"
                    src={makesenseIcon}
                    height="13"
                    width="13"
                    style={{ height: "13px", width: "auto" }}
                  />
                  <span>Offres sur Jobs_that_makesense</span>
            </div>
            )}
        </div>
    )
}

export default OrgFeatures
