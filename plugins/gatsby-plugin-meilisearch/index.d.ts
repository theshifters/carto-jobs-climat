import {IndexOptions, Settings} from "meilisearch";

export type  MeilisearchIndexQuery = {
    indexName: string,
    query: string,
    transformer: (resultUnit: any) => any,
    settings: Settings,
    sort: { key: string, order: "asc" | "desc" },
    options: IndexOptions
}

export type MeilisearchPluginIndexOptions = {
    mode: "INDEX"
    host?: string,
    apiKey: string,
    queries: MeilisearchIndexQuery[]
}

export type MeilisearchPluginPurgeIndexesOptions = {
    mode: "PURGE_INDEXES"
    host?: string,
    apiKey: string,
    numberOfIndexesToKeep?: number
}

export type MeilisearchPluginOptions = MeilisearchPluginIndexOptions | MeilisearchPluginPurgeIndexesOptions
