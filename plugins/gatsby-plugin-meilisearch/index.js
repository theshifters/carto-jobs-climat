const arg = require("arg");
const GatsbyMeilisearchUtils = require("./gatsby-utils")
const MeiliSearch = require("meilisearch");

const REQUIRED_PARAMETERS = ["--number-to-keep", "--api-key", "--host", "--mode"]

function parseArgumentsIntoOptions(rawArgs) {
  const args = arg(
    {
      "--mode": String,
      "--number-to-keep": Number,
      "--host": String,
      "--api-key": String
    },
    {
      argv: rawArgs.slice(2)
    }
  );
  REQUIRED_PARAMETERS.forEach(requiredParameter => {
    const currentRequiredParameterValue = args[requiredParameter];
    const requiredParameterIsNullOrUndefined = currentRequiredParameterValue == null || currentRequiredParameterValue === "";
    if (requiredParameterIsNullOrUndefined) {
      throw new Error(`Required parameter ${requiredParameter} is not present.`)
    }
  })
  if (args["--mode"] !== "purge") {
    throw new Error("Mode should be purge.")
  }
  if (args["--number-to-keep"] < 0) {
    throw new Error("Number of indexes to keep should be greater than 0.")
  }
  return {
    numberOfIndexesToKeep: args["--number-to-keep"],
    host: args["--host"],
    apiKey: args["--api-key"],
    mode: args["--mode"]
  };
}

const options = parseArgumentsIntoOptions(process.argv)

const meiliSearchInstance = new MeiliSearch({
  host: options.host,
  apiKey: options.apiKey
});

GatsbyMeilisearchUtils
  .purgeMeilisearchIndexes(options.numberOfIndexesToKeep, null, meiliSearchInstance)
  .then(() => process.exit(0))
  .catch((exception) => {
    console.log(`Error executing meilisearch plugin job ${options.mode}: ${exception}`)
    return process.exit(1);
  })
